# TravelTube

a graphic traveller into YouTube histories

* idea and graphics : Julia
* python code: Philippe

### Usage
1) install a Chrome Export History Plugin such as [this one](https://chrome.google.com/webstore/detail/export-historybookmarks-t/dcoegfodcnjofhjfbhegcgjgapeichlf)
2) export a JSON file of your history and upload it to [http://goulu.pythonanywhere.com/](http://goulu.pythonanywhere.com/)
3) Thank you ! :-)

### How it (will) work

The python builds a database of the YouTube videos all participants watched,

and Julia will make something cool with this ;-)

### Development
1) clone this project
2) pip install requirements.txt
3) main.py can be run as a regular python application
4) flask_app contains the website engine