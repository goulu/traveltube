from datetime import datetime
import json
import requests
import re
import pickle
import os
import logging

path = os.path.dirname(os.path.abspath(__file__))
cachef = path + '/database.pck'

database = {}  # dict of URL : record
try:
    database = pickle.load(open(cachef, "rb"))
except FileNotFoundError:
    pass

for d in database:
    database[d]['visits'] = 0

verify = False


def keywords(keys):
    res = set()
    for word in keys:
        word = ''.join(filter(lambda c: c.isalnum() or c.isspace(), word))
        word = word.lower()
        res = res.union(word.split(' '))
    return res


def find(r, s, i=0):
    start = r.find(s, i)
    if start < 0:
        pass
    else:
        start = start + len(s)
    return start


def youtube(url):
    logging.debug('requesting '+url)
    r = requests.get(url, verify=verify).text

    start = find(r, r'\"keywords\":[')
    if start < 0:
        keywords = []
    else:
        end = r.find(']', start)
        keywords = r[start:end].split(',')
        keywords = [s.replace(r'\"','') for s in keywords]
    logging.debug(keywords)

    return keywords


def load(filename):
    result = []

    with open(path + '/data/' + filename, encoding='utf-8') as file:
        data = json.load(file)
        for p in data:
            url = p['url']
            if not 'youtube.com/watch?v=' in url:
                continue

            if url in database:
                logging.debug(url + ' found in cache')
                p = database[url]

            else:
                # timestamp is Chrome  plugin dependent...
                try:
                    d = datetime.utcfromtimestamp(p['lastVisitTime'] / 1000)
                except:
                    d = datetime.strptime(p['lastVisitTimeUTC'], '%Y-%m-%dT%H:%M:%S.%fZ')

                p['lastVisitTime'] = d

                p['keywords'] = youtube(url)

                database[url] = p

            if 'keywords' in p:
                p['keywords'] = keywords(p['keywords'])
            else:
                p['keywords'] = []

            p.setdefault('visits', 0)
            p['visits'] += 1

            result.append(p)

    pickle.dump(database, open(cachef, "wb"))
    result.reverse()
    return result


def writeHTML(visits, filename):
    with open('results/' + filename, 'w', encoding='utf-8') as file:
        w = 100 // len(visits)
        file.write('<!doctype html>\n<html>\n<head>\n<meta charset="utf-8">\n</head>\n')
        file.write(
            '<style type="text/css">.chain {float: left; width:'+str(w) + '%;} img {width:200px;} .title {font-size:xx-small;} .multiple {color:red; font-weight:bold;}</style>')
        file.write('<body>\n')

        for row in visits:
            file.write('<div class="chain">')
            for d in row:
                file.write('<div class="cell">')
                if not d is None:
                    id = d['url'][-11:]
                    if d['visits'] > 1:
                        file.write('<span class="title multiple">')
                    else:
                        file.write('<span class="title">')
                    file.write(d['title'][:-10] + '</span></br>')
                    file.write('<a href="%s" target="_blank" download="%s">' % (d['url'], id))
                    file.write(
                        '<img src="https://img.youtube.com/vi/%s/hqdefault.jpg">' % id)
                    file.write('</a>')
                file.write('</div>')
            file.write('</div>\n')

        file.write('</body>\n</html>')


def writeJSON(visits):
    for i, chain in enumerate(visits):
        with open('results/chain%d.json' % (i + 1), 'w', encoding='utf-8') as file:
            json.dump(chain, file, indent=4, sort_keys=True, default=str)

    from itertools import zip_longest

    table = zip_longest(*visits)  # transpose
    table = list(map(list, table))
    with open('results/output.json', 'w', encoding='utf-8') as file:
        json.dump(table, file, indent=4, sort_keys=True, default=str)


def writeXLSX(visits):
    import xlsxwriter

    workbook = xlsxwriter.Workbook('results/output.xlsx')
    for chain in visits:
        worksheet = workbook.add_worksheet()

        def write(i, cells):
            for j, c in enumerate(cells):
                worksheet.write(i, j, c)

        write(0, ['title', 'url', 'visits', 'keywords'])
        for i, d in enumerate(chain):
            write(i + 1, [d['title'], d['url'], d['visits'], ','.join(d['keywords'])])
    workbook.close()


def parse_jsons():
    visits = []
    for filename in os.listdir(path + '/data'):
        if filename.endswith(".json"):
            visits.append(load(filename))

    # builds a dict of keywords frequencies
    keywords = dict()
    for v in visits:
        for d in v:
            for k in d['keywords']:
                if len(k) < 3: continue  # skip small words
                if k not in keywords:
                    keywords[k] = []
                keywords[k].append(d)

    keywords = [(k, len(v)) for k, v in keywords.items() if len(v) > 2]
    keywords.sort(key=lambda x: x[1], reverse=True)

    writeJSON(visits)
    writeXLSX(visits)
    writeHTML(visits, 'output.htm')
    for i, chain in enumerate(visits):
        writeHTML([chain],'chain %d.htm'%(i+1))
    return keywords


def autoloop(url='McbL844LPgo'):
    while True:
        urls, k = youtube(url)
        print(urls, k)
        url = urls[1]


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    keywords = parse_jsons()
    for k in keywords:
        print(k, end=' ')
