let table = d3.select('body').append('table'),
    thead = table.append('thead'),
    tbody = table.append('tbody');

function chain(i) {
    d3.json('results/output.json').then(function (data) {
        /*
        thead.append('tr')
            .selectAll('th')
            .data(columns).enter()
            .append('th')
            .text(function (column) {
                return column;
            });
        */

        // create a row for each object in the data
        let rows = tbody.selectAll('tr')
            .data(data)
            .enter()
            .append('tr');

        // create a cell in each row for each column
        let cells = rows.selectAll('td')
            .data(d => d)
            .enter()
            .append('td')
            .filter(d => d);

        cells.append('h5').text(d => d.title.substr(0, d.title.length - 9));
        cells.append('a').attr('href', d => d.url).attr('target', '_blank')
            .append('img')
            .attr('src', function (d) {
                let id = d.url;
                id = id.substr(id.length - 11);
                return 'https://img.youtube.com/vi/' + id + '/hqdefault.jpg'
            })
            .attr('width', '200px');

        return table;
    })
}

chain();
