let table = null, // all videos
    players = [], // the N*n players
    i = 0, // index in table of leftmost players
    n = 5,
    width = 200, xstep = width * 2;
height = 150, ystep = height * 1.1;


class Player {

    constructor(tag, i, j, d) {
        this.name = 'x' + i + 'j' + j;
        this.tag = tag.append('video')
        this.tag.style('width', width + 'px')
            .style('height', height + 'px')
            .style('top', ystep * j + 'px')
            .style('left', xstep * i + 'px')
            .style('background-color', 'grey')
            .attr('id', this.name);

        this.player = new YT.Player(this.name, {
            height: height,
            width: width,
            videoId: d.id,
            events: {
                'onReady': this.onPlayerReady
            }
        });
        this.done = false;
    }

    onPlayerReady(event) {
        event.target.playVideo();
    }

    load(id) {
        this.player.loadVideoById(id)
        this.player.start();
    }

}

function chain() {
    d3.json('../results/output.json').then(function (data) {
        table = data;
        data.forEach(function (chain, i) {
            let row = [];
            chain.forEach(function (d, j) {
                if (i < n) {
                    d.id = d.url.slice(-11);
                    let player = new Player(d3.select('body'), i, j, d);
                    row.push(player)
                }
            })
            players.push(row);
        })
    })
}

function onYouTubeIframeAPIReady() {
    chain();
}