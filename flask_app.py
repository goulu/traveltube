# http://goulu.pythonanywhere.com/

from flask import Flask, render_template, request
from werkzeug import secure_filename

import os, logging
from flask import Flask, Session, flash, request, redirect, url_for
from werkzeug.utils import secure_filename

import main as traveltube

ALLOWED_EXTENSIONS = set(['json'])

app = Flask(__name__,static_url_path='')
sess = Session()

app.config['DATA'] = 'data'
app.config['RESULTS'] = 'results'

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

@app.route('/uploads/<filename>')
def uploaded_file(filename):
    keywords=traveltube.parse_jsons(); # load(filename)
    return render_template('output.htm')

@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        # check if the post request has the file part
        if 'file' not in request.files:
            flash('No file part')
            return redirect(request.url)
        file = request.files['file']
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            flash('No selected file')
            return redirect(request.url)
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['DATA'], filename))
            return redirect(url_for('uploaded_file', filename=filename))

            return redirect(url_for('uploaded_file',filename=filename))
    return '''
    <!doctype html>
    <h1>TravelTube</h1>
    <ol>
    <li>install <a href="https://chrome.google.com/webstore/detail/export-historybookmarks-t/dcoegfodcnjofhjfbhegcgjgapeichlf" target="_blank">Chrome Export History</a> plugin</li>
    <li>export your history as JSON<li>
    <li>upload the JSON file here:
    <form method=post enctype=multipart/form-data>
      <input type=file name=file>
      <input type=submit value=Upload>
    </form><li>
    '''


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    app.secret_key = 'super secret key'
    app.config['SESSION_TYPE'] = 'filesystem'

    # sess.init_app(app)

    app.debug = True
    app.run(debug=True)